'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable(
    'users',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      email: {
        type: 'string',
        unique: true,
        notNull: true,
      },
      password: {
        type: 'string',
        notNull: true,
      },
      role: {
        type: 'string',
        notNull: true,
      },
      company_id: {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'users_company_id_fk',
          table: 'companies',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT',
          },
        },
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );

  db.createTable(
    'profiles',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      first_name: {
        type: 'string',
        notNull: false,
      },
      last_name: {
        type: 'string',
        notNull: false,
      },
      address: {
        type: 'string',
        notNull: false,
      },
      city: {
        type: 'string',
        notNull: false,
      },
      postal_code: {
        type: 'string',
        notNull: false,
      },
      user_id: {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'profiles_user_id_fk',
          table: 'users',
          mapping: 'id',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT',
          },
        },
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );

  db.createTable(
    'companies',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: 'string',
        notNull: true,
      },
      address: {
        type: 'string',
        notNull: false,
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );

  db.createTable(
    'projects',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: 'string',
        notNull: true,
      },
      description: {
        type: 'text',
        notNull: false,
      },
      company_id: {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'projects_company_id_fk',
          table: 'companies',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
          },
          mapping: 'id',
        },
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );
  db.createTable(
    'buildings',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: 'string',
        notNull: true,
      },
      description: {
        type: 'text',
        notNull: false,
      },
      project_id: {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'buildings_project_id_fk',
          table: 'projects',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
          },
          mapping: 'id',
        },
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );
  db.createTable(
    'gateways',
    {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true,
      },
      uuid: {
        type: 'string',
        unique: true,
        notNull: true,
      },
      installDate: {
        type: 'datetime',
        notNull: false,
      },
      building_id: {
        type: 'int',
        unsigned: true,
        notNull: false,
        foreignKey: {
          name: 'gateways_building_id_fk',
          table: 'buildings',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
          },
          mapping: 'id',
        },
      },
      created_at: {
        type: 'datetime',
        notNull: false,
      },
      updated_at: {
        type: 'datetime',
        notNull: false,
      },
    },
    callback,
  );
};

exports.down = function(db, callback) {
  db.removeForeignKey('profiles', 'profiles_user_id_fk', { dropIndex: true }, callback);
  db.removeForeignKey('users', 'users_company_id_fk', { dropIndex: true }, callback);
  db.removeForeignKey('projects', 'projects_company_id_fk', { dropIndex: true }, callback);
  db.removeForeignKey('buildings', 'buildings_project_id_fk', { dropIndex: true }, callback);
  db.removeForeignKey('gateways', 'gateways_building_id_fk', { dropIndex: true }, callback);

  db.dropTable('profiles', { ifExists: true }, callback);
  db.dropTable('users', { ifExists: true }, callback);
  db.dropTable('projects', { ifExists: true }, callback);
  db.dropTable('companies', { ifExists: true }, callback);
  db.dropTable('buildings', { ifExists: true }, callback);
  db.dropTable('gateways', { ifExists: true }, callback);
};

exports._meta = {
  version: 1,
};
