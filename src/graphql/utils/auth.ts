import { verify } from 'jsonwebtoken';
import { prisma, Prisma } from '../generated/prisma-client';
import * as jwt from 'jsonwebtoken';

export const APP_SECRET = process.env.APP_SECRET;

export interface Context {
  prisma: Prisma;
  request: any;
}

interface Token {
  id: number;
}

export async function getUser(req: any) {
  const Authorization = req.get('Authorization');

  if (Authorization) {
    const token = Authorization.replace('Bearer ', '');

    try {
      const verifiedToken = verify(token, APP_SECRET) as Token;

      if (verifiedToken) {
        const user = await prisma.user({ id: verifiedToken.id });
        return user;
      }
    } catch (err) {
      return false;
    }
  }

  return false;
}

export function sign(id: number) {
  return jwt.sign(
    {
      id,
    },
    APP_SECRET,
    {
      expiresIn: '30d',
    },
  );
}
