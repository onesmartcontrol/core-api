import { rule, shield, allow, and } from 'graphql-shield';

const rules = {
  isAuthenticated: rule()((parent, args, ctx) => {
    return Boolean(ctx.request.user);
  }),
  isAdmin: rule()((parent, args, ctx) => {
    return ctx.request.user.role === 'admin';
  }),
};

export const permissions = shield({
  Query: {
    '*': rules.isAuthenticated,
    users: and(rules.isAuthenticated, rules.isAdmin),
  },
  Mutation: {
    '*': rules.isAuthenticated,
    login: allow,
    signup: allow,
  },
});
