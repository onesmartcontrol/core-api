import * as bcrypt from 'bcryptjs';
import { sign } from '../utils';
import { stringArg, mutationType } from 'nexus';

export default mutationType({
  definition(t) {
    t.field('signup', {
      type: 'AuthPayload',
      args: {
        email: stringArg(),
        password: stringArg(),
        role: stringArg({
          nullable: true,
          default: 'admin',
        }),
      },
      resolve: async (_, { email, password, role }, ctx) => {
        const user = await ctx.prisma.createUser({
          email,
          password: await bcrypt.hash(password, 10),
          role,
        });

        return {
          token: sign(user.id),
          user,
        };
      },
    });
    t.field('login', {
      type: 'AuthPayload',
      args: {
        email: stringArg(),
        password: stringArg(),
      },
      resolve: async (_, { email, password }, context) => {
        const user = await context.prisma.user({ email });

        if (!user) {
          throw new Error(`No user found for email: ${email}`);
        }

        const passwordValid = await bcrypt.compare(password, user.password);

        if (!passwordValid) {
          throw new Error('Invalid password');
        }

        return {
          token: sign(user.id),
          user,
        };
      },
    });
  },
});
