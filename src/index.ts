require('dotenv').config();

import * as path from 'path';
import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { makePrismaSchema } from 'nexus-prisma';
import { applyMiddleware } from 'graphql-middleware';

/*
 * Prisma
 */
import { prisma } from './graphql/generated/prisma-client';
import datamodelInfo from './graphql/generated/nexus-prisma';

/*
 * Graphql
 */
import { permissions } from './graphql/permissions';
import { getUser } from './graphql/utils';
import ObjectTypes from './graphql/object-types';
import Models from './graphql/models';
import Mutations from './graphql/mutations';

/*
 * REST
 */
import applyRoutes from './rest/routes';

let app = express();

let schema = makePrismaSchema({
  types: [Models, ObjectTypes, Mutations],

  prisma: {
    datamodelInfo,
    client: prisma,
  },

  outputs: {
    schema: path.join(__dirname, './graphql/generated/schema.graphql'),
    typegen: path.join(__dirname, './graphql/generated/nexus.ts'),
  },
});
schema = applyMiddleware(schema, permissions); // protect our graphql routes with permissions

const server = new ApolloServer({
  context: async ({ req, res }) => ({
    prisma,
    request: {
      ...req,
      user: await getUser(req),
    },
    response: res,
  }),
  schema,
});

app = applyRoutes(app); // here we apply all of our custom routes (rest or other)
server.applyMiddleware({ app });

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`));
