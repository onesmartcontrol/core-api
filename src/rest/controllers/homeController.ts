import { prisma } from '../../graphql/generated/prisma-client';

export async function homeRoute(req, res) {
  const user = await prisma.user({ id: 1 });

  return res.send('This is a custom rest route that uses prisma to fetch data!! ' + (user ? user.email : ''));
}
