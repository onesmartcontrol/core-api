import { homeRoute } from '../controllers/homeController';

// define all routes here
export default function applyRoutes(app) {
  app.get('/', homeRoute);

  return app;
}
