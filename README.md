# Basic commands
## migrations
### DEV
- **yarn run migrate-create ....** for creating local migrations
- **yarn run migrate-dev-up** when migrating to your local db
- **yarn run migrate-dev-down** when rolling back migrations from your local db

### PROD
- TODO

## deployment
### dev
never run prisma deploy because it will update your database based on your models. Which would later on give you problems with db-migrate.

- **yarn run deploy**